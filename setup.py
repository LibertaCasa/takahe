#!/usr/bin/env python

import setuptools
package = 'takahe'
python_requires = '>=3.10'

requires = []
test_requirements = []

foundpackages=setuptools.find_packages(where=".")
prefixedpackages=prefixed=[package + '.' + item for item in foundpackages]

setuptools.setup(
    name='Takahe',
    version='300ccfbfac970c25eb73922687ded719750acc90',
    description='An ActivityPub/Fediverse server',
    long_description_content_type='text/markdown',
    author='Takahe Authors',
    url='https://jointakahe.org/',
    packages=prefixedpackages,
    scripts=["manage.py"],
    package_data={'': ['LICENSE']},
    package_dir= {
        'takahe.activities': './activities',
        'takahe.core': './core',
        'takahe.mediaproxy': './mediaproxy',
        'takahe.stator': './stator',
        'takahe.users': './users',
        'takahe.takahe': './takahe',
        'takahe.api': './api'
    },
    include_package_data=True,
    python_requires=python_requires,
    install_requires=[
'bleach~=5.0.1',
'blurhash-python~=1.1.3',
'cryptography~=38.0',
'dj_database_url~=1.0.0',
'django-cache-url~=3.4.2',
'django-htmx~=1.13.0',
'django-storages[google,boto3]~=1.13.1',
'django~=4.1',
'email-validator~=1.3.0',
'gunicorn~=20.1.0',
'httpx~=0.23',
'markdown_it_py~=2.1.0',
'pillow~=9.3.0',
'psycopg2~=2.9.5',
'pydantic~=1.10.2',
'pyld~=2.0.3',
'pylibmc~=1.6.3',
'pymemcache~=4.0.0',
'python-dotenv~=0.21.0',
'redis~=4.4.0',
'sentry-sdk~=1.11.0',
'sphinx~=5.3.0',
'urlman~=2.0.1',
'uvicorn~=0.19',
'whitenoise~=6.2.0'
],
    license='BSD-3',
    zip_safe=False,
    classifiers=[
        'Programming Language :: Python :: 3.10',
    ],
)
